# test_kits.py
#
# Fab electronics inventory test script.
# Tests if parts in kit files map to real parts in the inventory.
#
# Created by Krisjanis Rijnieks 20 Jul 2020
# (c) Krisjanis Rijnieks 2020
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.

import os
import toml

input_dir = "../kits"
inventory_file = "../inventory.toml"

def error(message):
  global error_count
  print('\033[1;31;40mERROR:\033[0;37;40m', message)

def test_field(parsed_toml, category, part, field):
  try:
    print('   [x] ' + field + ': ' + str(parsed_toml[category][part][field]))
  except Exception as ex:
    error('Part ' + field + ' error')
    exit(1)
  if parsed_toml[category][part][field] == '':
    error('Field ' + field + ' is empty')
    exit(1)

def test_structure(parsed_toml):
  print('\nTesting structure...')
  for category in parsed_toml:
    if category == 'meta':
      continue
    print('\nCategory: ' + category)
    print('Parts:')
    for part in parsed_toml[category]:
      print(' - ' + part)
      test_field(parsed_toml, category, part, 'quantity')

def find_part(inventory_toml, partid):
  for category in inventory_toml:
    if  category == 'meta':
      continue
    for part in inventory_toml[category]:
      if part == partid:
        return
  error('Part not found in inventory')
  exit(1)

def test_mapping(inventory_toml, parsed_toml):
  print('\nTesting mapping...')
  for category in parsed_toml:
    if category == 'meta':
      continue
    print('\nCategory: ' + category)
    print('Parts:')
    for part in parsed_toml[category]:
      print(' - ' + part)
      find_part(inventory_toml, part)

# Check if directory exists
dir_exists = os.path.isdir(input_dir)
if dir_exists == False:
  error('Directory ' + input_dir + ' does not exist')
  exit(1)

# Check if inventory file exists
# Check if path exists
inventory_file_exists = os.path.exists(inventory_file)
if inventory_file_exists != True:
  error('Inventory file does not exist')
  exit(1)

# Try to parse inventory TOML file
try:
  inventory_toml = toml.load(inventory_file)
except Exception as ex:
  error('TOML Error: ' + str(ex))
  exit(1)

# List toml files in directory 
input_files = []
for f in os.listdir(input_dir):
  path = os.path.join(input_dir, f)
  if os.path.isfile(path):
    input_files.append(path)

# Test each file
for f in input_files:
  print('\nParsing ' + f)
  try:
    parsed_toml = toml.load(f)
  except Exception as ex:
    error('TOML Error: ' + str(ex))
    exit(1)
  test_structure(parsed_toml)
  test_mapping(inventory_toml, parsed_toml)

# We are fine. Exit with no errors.
print('\n' + input_dir + ' is \033[1;32;40mOK!')
exit(0)
