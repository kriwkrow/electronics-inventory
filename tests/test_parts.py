# test_parts.py
#
# Fab electronics inventory test script.
# Tests if inventory file is parseable and parts have all fields set.
#
# Created by Krisjanis Rijnieks 19 Jul 2020
# (c) Krisjanis Rijnieks 2020
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.

import os
import toml

input_file = "../inventory.toml"

def error(message):
  global error_count
  print('\033[1;31;40mERROR:\033[0;37;40m', message)

def test_meta(parsed_toml):
  print('Checking if meta field exists...')
  try: 
    print(parsed_toml['meta'])
  except Exception as ex:
    error('Could not find meta field')
    exit(1)

def test_field(parsed_toml, category, part, field):
  try:
    print('   [x] ' + field + ': ' + str(parsed_toml[category][part][field]))
  except Exception as ex:
    error('Part ' + field + ' error')
    exit(1)
  if parsed_toml[category][part][field] == '':
    error('Field ' + field + ' is empty')
    exit(1)

def test_unique(part_array, part_id):
  if part_id in part_array:
    error('Part ' + part_id + ' is not unique')
    exit(1)

def test_parts(parsed_toml):
  part_ids = []
  print('\nTesting parts...')
  for category in parsed_toml:
    if category == 'meta':
      continue
    print('\nCategory: ' + category)
    print('Parts:')
    for part in parsed_toml[category]:
      print(' - ' + part)
      test_unique(part_ids, part)
      part_ids.append(part)
      test_field(parsed_toml, category, part, 'name')
      test_field(parsed_toml, category, part, 'description')
      test_field(parsed_toml, category, part, 'manufacturer')
      test_field(parsed_toml, category, part, 'partno')
      test_field(parsed_toml, category, part, 'datasheet')
      test_field(parsed_toml, category, part, 'package')

# Check if path exists
input_file_exists = os.path.exists(input_file)
if input_file_exists != True:
  error('Input file does not exist')
  exit(1)

# Try to parse TOML file
try:
  parsed_toml = toml.load(input_file)
except Exception as ex:
  error('TOML Error: ' + str(ex))
  exit(1)

test_meta(parsed_toml)
test_parts(parsed_toml)

# We are fine. Exit with no errors.
print('\n' + input_file + ' is \033[1;32;40mOK!')
exit(0)
