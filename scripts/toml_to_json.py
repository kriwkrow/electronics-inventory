# toml_to_json.py
#
# Fab Inventory helper script.
# Converts TOML files to JSON for easier parsing on the frontend side.
#
# Created by Krisjanis Rijnieks 20 Jul 2020
# (c) Krisjanis Rijnieks 2020
#
# This work may be reproduced, modified, distributed,
# performed, and displayed for any purpose, but must
# acknowledge this project. Copyright is retained and
# must be preserved. The work is provided as is; no
# warranty is provided, and users accept all liability.

import os
import toml
import json

inventory_file = "../inventory.toml"
shops_dir = "../shops"
kits_dir = "../kits"

def error(message):
  global error_count
  print('\033[1;31;40mERROR:\033[0;37;40m', message)

def convert_file(toml_file_path):
  print('\nConverting ' + toml_file_path)
  try:
    parsed_toml = toml.load(toml_file_path)
  except Exception as ex:
    error('TOML Error: ' + str(ex))
    exit(1)
  json_file_path = toml_file_path[:-5] + '.json'
  with open(json_file_path, 'w', encoding='utf-8') as f:
    json.dump(parsed_toml, f, ensure_ascii=False, indent=4)
  print('JSON file created: ' + json_file_path)

def create_index_file(target_dir, toml_files):
  json_files = []
  for file in toml_files:
    json_file = file[:-5] + '.json'
    json_files.append(json_file)
  index_file = os.path.join(target_dir, 'index.json')
  with open(index_file, 'w', encoding='utf-8') as f:
    json.dump(json_files, f, ensure_ascii=False, indent=4)
    print('Created index file: ' + index_file)

# Check if inventory file exists
inventory_file_exists = os.path.exists(inventory_file)
if inventory_file_exists != True:
  error('Inventory file does not exist')
  exit(1)

# Check if shops dir exists
shops_dir_exists = os.path.isdir(shops_dir)
if shops_dir_exists == False:
  error('Directory ' + shops_dir + ' does not exist')
  exit(1)

# Check if shops dir exists
kits_dir_exists = os.path.isdir(kits_dir)
if kits_dir_exists == False:
  error('Directory ' + shops_dir + ' does not exist')
  exit(1)

print('Converting all TOML files to JSON...')

# Convert inventory file 
convert_file(inventory_file)

# Convert shops files
shops = []
for f in os.listdir(shops_dir):
  path = os.path.join(shops_dir, f)
  if os.path.isfile(path):
    if path[-5:] == '.toml':
      convert_file(path)
      shops.append(f)

# Create JSON index file for shops entries
create_index_file(shops_dir, shops)

# Convert kits files
kits = []
for f in os.listdir(kits_dir):
  path = os.path.join(kits_dir, f)
  if os.path.isfile(path):
    if path[-5:] == '.toml':
      convert_file(path)
      kits.append(f)

# Create JSON index file for kits entries
create_index_file(kits_dir, kits)

# We are fine. Exit with no errors.
print('\nTOML to JSON convert \033[1;32;40mSUCCESS!')
exit(0)