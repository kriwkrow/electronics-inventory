# Fab Electronics Inventory

Electronics inventory defined in TOML and to be managed with Git.

> [View Inventory](http://pub.fabcloud.io/inventory/electronics/)

## Introduction

Every electronics component is defined as an entry in the `inventory.toml` file. Edits are added locally and then pushed here with `git push`. Once GitLab receives a push, tests are run to check if the components are added correctly. If tests pass - a new version of HTML dashboard is published.

## Convention

- Use manufacturer name and part number in `inventory.toml` file.
- Name the part according to the pattern "PartType Value [Unit] Footprint".
- In TOML: "[parttype.valueUnitFootprint]".
- Make sure you run `tests/test_parts.py` before commiting and pushing.

## Automatic Shopping Cart Generation

At the moment this is thought to be achieved by adding a descriptor file for each of the shops. We start with DigiKey (`shops/digikey.toml`) as the current spreadsheet-based inventory has all the numbers. This can be done for each shop that supports automatic cart generation.

## TODO

- [x] Establish an initial markup and naming convention
- [x] Create simple web interface to test part-to-shop mapping
- [ ] Add all the electronics components from the [existing Fab inventory](http://fab.cba.mit.edu/about/fab/inv.html) sheet
- [ ] Change shops test to check that all inventory parts are covered with shop part numbers
- [ ] Add unique manufacturer number test
- [ ] Map them with DigiKey part numbers
- [ ] Figure out kit description (levels: individual, academy, lab)

## FAQ

### Why TOML?

> Because it is most human-friendly to read as JSON or YAML and can be parsed as easily.

###  Why one file per shop?

> All shops have the same manufacturer numbers for the parts, but their shop product numbers are different. If we want to target specific shops, we need to use their specific product numbers.

### Why this and not database driven solution?

> This is as open source as it can possibly get. GitLab has amazing automatic test and deploy features and it just makes plain sense to use them. It is much easier to test and debug this as a black-box database solution somewhere on a random server.
